//Setup the dependencies

const express = require("express");
const mongoose = require("mongoose");

const app = express();
const port = 3000;
app.use(express.json());

const taskRoute = require("./routes/taskRoute")

//Database Connection
mongoose.connect("mongodb+srv://jarlosjoseph:admin123@batch204-jarlosjoseph.ahjdzv3.mongodb.net/B204-to-dos?retryWrites=true&w=majority", 
	{
	useNewUrlParser: true,
	useUnifiedTopology: true
	}
);
let db = mongoose.connection;

// If a connection error occured, output in the console
db.on("error", console.error.bind(console, "Connection Error"));

// If the connection is successful, output in the console
db.once("open", () => console.log("We're connected to the cloud database."));


// Add the task route
app.use("/tasks", taskRoute);
//localhost:3000/tasks/





//Server Listening
app.listen(port, () => console.log (`Now listening to port ${port}`));

